
import {Routes, Route, Link} from 'react-router-dom'
import { Navbar } from '../Components/Navbar'
import { About } from '../Components/About';
import { Contact } from '../Components/Contact';
import { Services } from '../Components/Services';
import { Home } from '../Components';
import Footer from '../Footer/Footer';

function App(){
    return(
        <div className="container"> 
        
          <Navbar/>
         {/* <Footer /> */}
         <Routes>
          <Route path="/" element={<Home />}/>
          <Route path="/about" element={<About/>} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/services" element={<Services />} />
         </Routes>
        </div>
    );
}
export default App
