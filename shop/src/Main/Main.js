import './Main.css'
import acticle from '../data/article.json'

function Main(){
    return(
        <div className="Main">
            {acticle.map(item=>
                <section>
                    <h1>{item.title}</h1>
                    <h2>{item.body}</h2>
                </section>)}
        </div>
    );
}
export default Main